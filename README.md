# Country data application

## How to use

To run the server, make sure you have Maven installed and run:
```
./mvnw spring-boot:run
```

You can now request e.g. `GET http://localhost:8080/country/Denmark` or `GET http://localhost:8080/countries`.

To run the tests:
```
./mvnw test
```
