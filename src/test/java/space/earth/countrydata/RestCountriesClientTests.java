package space.earth.countrydata;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.web.reactive.function.client.WebClient;

class RestCountriesClientTests {

  public static MockWebServer mockWebServer;
  private RestCountriesClient countriesClient;

  @BeforeEach
  public void setup() throws IOException {
    mockWebServer = new MockWebServer();
    mockWebServer.start();
    countriesClient =
        new RestCountriesClient(WebClient.builder(), mockWebServer.url("/").toString());
  }

  @Test
  void countriesList() {
    MockResponse mockResponse =
        new MockResponse()
            .addHeader("Content-Type", "application/json; charset=utf-8")
            .setBody(
                "[{\"alpha2Code\": \"FI\", \"name\":\"Finland\"}, {\"alpha2Code\": \"DK\","
                    + " \"name\":\"Denmark\"}]");

    mockWebServer.enqueue(mockResponse);

    Country[] countries = countriesClient.getAllCountries().block();

    assertEquals(2, countries.length);
    assertEquals("FI", countries[0].getCode());
    assertEquals("Finland", countries[0].getName());

    assertEquals("DK", countries[1].getCode());
    assertEquals("Denmark", countries[1].getName());
  }

  @Test
  void country() {
    MockResponse mockResponse =
        new MockResponse()
            .addHeader("Content-Type", "application/json; charset=utf-8")
            .setBody(
                "[{\"alpha2Code\": \"DK\", \"name\":\"Denmark\", \"population\": 5000000, \"flag\":"
                    + " \"https://url.to/flag\", \"capital\": \"Copenhagen\"}]");

    mockWebServer.enqueue(mockResponse);

    DetailedCountry country = countriesClient.getCountry("Denmark").block();

    assertEquals("DK", country.getCode());
    assertEquals("Denmark", country.getName());
    assertEquals("Copenhagen", country.getCapital());
    assertEquals(5000000, country.getPopulation());
    assertEquals("https://url.to/flag", country.getFlagFileUrl());
  }
}
