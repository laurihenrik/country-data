package space.earth.countrydata;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

public class Country {

  @JsonProperty("country_code")
  @JsonAlias("alpha2Code")
  @Getter
  @Setter
  private String code;

  @Getter @Setter private String name;
}
