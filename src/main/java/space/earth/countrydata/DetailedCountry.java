package space.earth.countrydata;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

public class DetailedCountry extends Country {

  @Getter @Setter private String capital;

  @Getter @Setter private int population;

  @JsonProperty("flag_file_url")
  @JsonAlias("flag")
  @Getter
  @Setter
  private String flagFileUrl;
}
