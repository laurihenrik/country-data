package space.earth.countrydata;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Component
public class RestCountriesClient {

  private final WebClient client;

  public RestCountriesClient(
      WebClient.Builder builder, @Value("${restcountries.url}") String baseUrl) {
    this.client = builder.baseUrl(baseUrl).build();
  }

  public Mono<Country[]> getAllCountries() {
    return this.client
        .get()
        .uri("/all?fields=name;alpha2Code")
        .accept(MediaType.APPLICATION_JSON)
        .retrieve()
        .bodyToMono(Country[].class);
  }

  public Mono<DetailedCountry> getCountry(String name) {
    return this.client
        .get()
        .uri("/name/" + name + "?fields=name;alpha2Code;flag;population;capital")
        .accept(MediaType.APPLICATION_JSON)
        .retrieve()
        .bodyToMono(DetailedCountry[].class)
        .map(
            countries -> {
              return countries[0];
            });
  }
}
