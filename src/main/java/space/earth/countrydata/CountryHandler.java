package space.earth.countrydata;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

@Component
public class CountryHandler {

  @Autowired private ApplicationContext context;

  public Mono<ServerResponse> country(ServerRequest request) {
    String requestCountryName = request.pathVariable("name");
    RestCountriesClient countriesClient = context.getBean(RestCountriesClient.class);
    return countriesClient
        .getCountry(requestCountryName)
        .flatMap(
            country -> {

              // We're not providing a search-like API like restcountries.eu does.
              if (!country.getName().toLowerCase().equals(requestCountryName.toLowerCase())) {
                return ServerResponse.notFound().build();
              }

              return ServerResponse.ok()
                  .contentType(MediaType.APPLICATION_JSON)
                  .body(BodyInserters.fromValue(country));
            })
        .onErrorResume(error -> ServerResponse.notFound().build());
  }

  public Mono<ServerResponse> countryList(ServerRequest request) {
    RestCountriesClient countriesClient = context.getBean(RestCountriesClient.class);
    return countriesClient
        .getAllCountries()
        .flatMap(
            countries -> {
              return ServerResponse.ok()
                  .contentType(MediaType.APPLICATION_JSON)
                  .body(BodyInserters.fromValue(new Countries(countries)));
            });
  }
}
