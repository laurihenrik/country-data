package space.earth.countrydata;

import static org.springframework.web.reactive.function.server.RequestPredicates.GET;
import static org.springframework.web.reactive.function.server.RequestPredicates.accept;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

@Configuration(proxyBeanMethods = false)
public class CountryRouter {

  @Bean
  public RouterFunction<ServerResponse> routes(CountryHandler countryHandler) {
    return RouterFunctions.route(
            GET("/country/{name}").and(accept(MediaType.APPLICATION_JSON)), countryHandler::country)
        .and(
            RouterFunctions.route(
                GET("/countries").and(accept(MediaType.APPLICATION_JSON)),
                countryHandler::countryList));
  }
}
