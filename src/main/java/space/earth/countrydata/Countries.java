package space.earth.countrydata;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Countries {
  @JsonProperty("countries")
  private Country[] countries;

  public Countries(Country[] countries) {
    this.countries = countries;
  }
}
